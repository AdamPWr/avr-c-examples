/*
 * Func.c
 *
 * Created: 10/17/2019 6:10:39 PM
 *  Author: Adam
 */ 

#include"Func.h"
#include<avr/delay.h>

bool bitIsSet(int* PIN,int PBX)
{
	return (*PIN & (1<<PBX)) != 0 ;
}

bool checkIfButtonIsPressed(int* PIN,int p_STOP_BUTTON)
{
	if(!bitIsSet(PIN,p_STOP_BUTTON))
	{
		_delay_ms(90);
		return true;
	}
	return false;
}

void toggleElement(int* PORT,int ELEMENT)
{
	//PORT ^= (1<<p_BUZZER);
	*PORT ^= (1<<ELEMENT);
}

