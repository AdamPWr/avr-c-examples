/*
 * Func.h
 *
 * Created: 10/17/2019 6:06:45 PM
 *  Author: Adam
 */ 


#ifndef FUNC_H_
#define FUNC_H_

#include <stdbool.h>
#define F_CPU 1000000
bool bitIsSet(int* PIN,int PBX);
bool checkIfButtonIsPressed(int* PIN,int p_STOP_BUTTON);
void toggleElement(int* PORT,int ELEMENT);

#endif /* FUNC_H_ */