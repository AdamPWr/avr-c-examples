/*
użycie tiera 0 w trybie ctc tzn sam sie zeruje po doliczeniu do odpowiedniej wartosci
 */ 

#define  F_CPU 1000000
#define LED1 PB4
#define LED2 PB3

#include <avr/io.h>
#include <avr/delay.h>
#include <avr/interrupt.h>

int times = 0;

int main(void)
{
    DDRB  |= (1<<LED1) | (1<<LED2);
	TCCR0A |= (1<<WGM01); // ustawienie zerowania timera po dopasowaniu do porzadanej wartosci 
	OCR0A |= 195;			//ustawienie wartosci do porownywania
	TIMSK |=  (1<<OCIE0A); // zezwolenie na przerwania 
	TCCR0B |= (1<<CS00) | (1<<CS02); //startuje z preskalerem preskalera na /1024
	sei();
	
    while (1) 
    {
		PORTB ^= (1<<LED2);
		_delay_ms(1000);
    }
}

ISR(TIMER0_COMPA_vect)
{
	++times;
	if(times >= 5)
	{
		times = 0;
		PORTB ^= (1<<LED1);
	}
}