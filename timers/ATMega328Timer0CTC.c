/*
 * Timer0ATMega328p.c
 *
 * Created: 11/23/2019 1:29:23 PM
 * Author : Adam
 */ 

#define F_CPU 1000000

#include "Func.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/delay.h>

#define LED PB0

int counter=0;

int main(void)
{
	DDRB |= (1<<LED);
	
    OCR0A = 195; //przy 1MHZ daje ok. 0.2s
	TCCR0A |= (1<<WGM01); //ustawienie trybu ctc 
	TIMSK0 |= (1<<OCIE0A); 
	sei();
	TCCR0B |= (1<<CS00) | (1<<CS02); // preskaler i start
    while (1) 
    {
		_delay_ms(1000);
		//toggleElement(&PORTB,LED);
    }
}

ISR(TIMER0_COMPA_vect)
{
	if(counter<5)
	{
		++counter;
	}
	else
	{
		toggleElement(&PORTB,LED);
		counter = 0;
	}

}

