/*
-GIMSK aktywujemy przerwania typu PCINT
-PCMSK definiujemy na jakich pinach nasłuchiwać
sei() globalna obsługa przerwań
*/
#define LED PB3
#define F_CPU 1000000

#include <avr/io.h>
#include<avr/interrupt.h>

#define F_CPU 1000000

int main(void)
{

	DDRB |= (1<<LED) | (1<<BUZZER);
	DDRB &= ~(1<<STOP_BUTTON);
	PORTB |= (1<<STOP_BUTTON);		//pull-up resistor
	
	cli();						// wyłączenie przerwań
	GIMSK |= (1<<PCIE);			// aktywowanie przerwań zewnętrznych
	PCMSK |= (1<<STOP_BUTTON);	// aktywowanie przerwan na pinie PB4
	sei();
    while (1) 
    {
		
    } 
}

ISR(PCINT0_vect)
{
	cli();
	PORTB |= (1<<LED);
	sei();
}

