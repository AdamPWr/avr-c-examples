/*
PCICR na kt�rych partach nas�uchiwa�
PCMSK(numer_portu) na jakich pinach nas�uchiwa� przerwa�
sei() globalna obs�uga przerwa�
 */ 
#define LED0 PB0
#define LED1 PB1
#define BUTTON PC3
#define F_CPU 1000000
#include <avr/io.h>
#include<avr/delay.h>
#include<avr/interrupt.h>
#include"Func.h"

ISR(PCINT1_vect)
{	
	cli();
	toggleElement(&PORTB,LED0);
	_delay_ms(100);
	sei();

}
int main(void)
{
    	DDRB |= (1<<LED0) | (1<<LED1);
	DDRB &= ~(1<<BUTTON);			// pullup resistor na przycisku
	PORTB |=(1<<BUTTON);
	
	PCICR |= (1<<PCIE1);			//ustawia prerwaniaPCINT  na PINC 
	PCMSK1 |= (1<<BUTTON);			//ustawia  na kt�rych pinach portu wybranego wy�ej nas�uchiwa� przerwa�
	
	PINB|= (1<<LED0) | (1<<LED1); 		// zapala obie diody
	sei();
    while (1) 
    {
		toggleElement(&PORTB,LED1);
		_delay_ms(300);
    }
}

